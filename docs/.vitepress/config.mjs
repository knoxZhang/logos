import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base: "/logos",
  title: "Logos",
  description: "为开发人员/DevOps/极客精选的 SVG 图标集合",
  lastUpdated: true,
  lang: "zh-cn",
  head: [["link", { rel: "icon", href: "https://avatars.githubusercontent.com/u/22465727?s=400&u=de7c25b2a6d12d1e9288c6898176a2054b30b0f3&v=4" }]],
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    search: {
      provider: 'local',
      options: {
        locales: {
          zh: {
            translations: {
              button: {
                buttonText: '搜索文档',
                buttonAriaLabel: '搜索文档'
              },
              modal: {
                noResultsText: '无法找到相关结果',
                resetButtonTitle: '清除查询条件',
                footer: {
                  selectText: '选择',
                  navigateText: '切换'
                }
              }
            }
          }
        }
      }
    },
    docFooter: {
      prev: "上一页",
      next: "下一页",
    },
    // nav: [
    //   { text: 'Home', link: '/' },
    //   { text: 'Examples', link: '/markdown-examples' }
    // ],

    sidebar: [
      // demo
      // {
      //   text: 'Examples',
      //   items: [
      //     { text: 'Markdown Examples', link: '/markdown-examples' },
      //     { text: 'Runtime API Examples', link: '/api-examples' }
      //   ]
      // }
      {
        text: '前端',
        items: [
          { text: 'Javascript', link: '/front-end/javascript' },
          { text: 'CSS', link: '/front-end/css' },
          { text: 'HTML', link: '/front-end/html' }
        ]
      }

    ],

    socialLinks: [
      {
        icon: {
          svg: `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">  <image id="image0" width="16" height="16" x="0" y="0"
                    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
                AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAdVBMVEUAAADbABjaABTaABPZ
                ABT/ACvZABXaABPZABTZABPjSVb2yMz9+Pj////88fLkSlf++/v3y871wcXhPErfLDvdITH++vrj
                RVLbDh/cFifhOEb++fnnX2r99vboanXhOUf0t7z1wsb+/f3iPkz99/f2xcniP003qpK8AAAACXRS
                TlMAK4/V8waU/b0v8seaAAAAAWJLR0QN9rRh9QAAAAd0SU1FB+YJEQElEWr9mpYAAACKSURBVBjT
                ZY9ZEsIwDEMdJ00qqEvZy1qgwP2PiJ0CM9D3Z83IkogUxz4Ez44GipiQSbHId4kvpSkRmEwrMWpE
                9SfM5E2D5IiBuSyWK2MNMHlgI1u0O6UFPAVAZI+DWSog/Ahigh8EtRzlZBY24awfu4tc7anGflJv
                vcVqsTqf98cTVmxcfTTub/4LPbIKJ1fQ5EgAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMDktMTdU
                MDE6Mzc6MTcrMDA6MDApdZt0AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIyLTA5LTE3VDAxOjM3OjE3
                KzAwOjAwWCgjyAAAACh0RVh0ZGF0ZTp0aW1lc3RhbXAAMjAyMi0wOS0xN1QwMTozNzoxNyswMDow
                MA89AhcAAAAASUVORK5CYII=" />
                </svg>`,
        },
        link: "https://gitee.com/knoxZhang/logos",
      },
    ]
  }
})
