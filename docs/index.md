---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Logos"
  text: "为开发人员/DevOps/极客精选的 SVG 图标集合"
  tagline: 简单、强大、快速、好用
  actions:
    - theme: brand
      text: 快速查看
      link: /front-end/javascript
    # - theme: alt
    #   text: API Examples
    #   link: /api-examples

# features:
#   - title: Feature A
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature B
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature C
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

